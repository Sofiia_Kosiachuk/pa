import data.proxy.MySqlClientsProtectionProxy
import domain.entity.users.User
import domain.entity.users.UserRole

private const val TRAINER_ID = 782633144
private const val OFFSET = 0
private const val ROW_COUNT = 20

suspend fun main() {

    val clientsProxy = MySqlClientsProtectionProxy()

    val users = mutableListOf<User>()
    repeat(10) { i ->
        users.add(User(
            id = i,
            name = "name $i",
            surname = "surname $i",
            login = "login $i",
            password = "12345678",
            phoneNumber = "0996661313",
            email = "email $ i",
            userRole = UserRole.USER
        ))
    }

    clientsProxy.run {
//        users.forEach { create(it) }
        println("Getting users first time:")
        var usersByTrainerId = getByTrainerId(TRAINER_ID, OFFSET, ROW_COUNT)
        usersByTrainerId.forEach { println(it) }

        println("Getting users second time:")
        usersByTrainerId = getByTrainerId(TRAINER_ID, OFFSET, ROW_COUNT)
        usersByTrainerId.forEach { println(it) }
    }
}