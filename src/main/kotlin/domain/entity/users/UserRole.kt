package domain.entity.users

enum class UserRole {
    ADMIN,
    USER,
}